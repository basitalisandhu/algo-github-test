import pytest
from python_test import main

parse_commands, copy_commands, functional_commands, random_commands = main()


def test_parse_commands():
    parse_command_test = parse_commands
    assert parse_command_test == [
        {'function': 'parse', 'help': 'file help', 'value': 'file'}]


def test_copy_commands():
    copy_commands_test = copy_commands
    assert copy_commands_test == [
        {'function': 'copy', 'help': 'copy help', 'value': 'file'}]


def test_functional_commands():
    functional_commands_test = functional_commands
    assert functional_commands_test == [{'function': 'parse', 'help': 'file help', 'value': 'file', '_list': 'parse', '_counter': 1}, {
        'function': 'copy', 'help': 'copy help', 'value': 'file', '_list': 'copy', '_counter': 1}]


def test_random_commands():
    random_commands_test = random_commands
    assert len(random_commands_test) == 2
